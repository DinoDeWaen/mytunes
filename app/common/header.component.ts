/**
 * Created by serrut on 05/06/16.
 */
import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES, ROUTER_PROVIDERS, Router} from '@angular/router';
import {User} from "../user/models/user";
import {UserService} from "../user/services/user.srv";

@Component({
    moduleId: module.id,
    selector: 'header-component',
    templateUrl: 'header.component.html',
    providers: [UserService],
    directives: [ROUTER_DIRECTIVES]
})

export class HeaderComponent {

    public user:User;

    // Constructors

    constructor(private userService:UserService, private router:Router) {
        this.userService.getUser()
            .subscribe(user => {
                this.user = user;
            });
    }

    // Actions

    logout() {
        this.userService.logout()
            .subscribe(() => {
                this.router.navigate(["/"]);
            });
    }
}