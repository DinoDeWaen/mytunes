/**
 * Created by serrut on 05/06/16.
 */
import {Component} from '@angular/core';


@Component({
    moduleId: module.id,
    selector: 'welcome-component',
    templateUrl: 'welcome.component.html'
})

export class WelcomeComponent {

    public showMore:boolean = false;

    // Actions

    onShowMore() {
        this.showMore = true;
    }

}