/**
 * Created by serrut on 05/06/16.
 */
import {Component} from '@angular/core';
import {Album} from "./models/album";
import {AlbumImageSize} from "./models/album";
import {MusicService} from "./services/music.srv";
import {RouteSegment, ROUTER_DIRECTIVES} from '@angular/router';
import {Song} from "./models/song";

@Component({
    moduleId: module.id,
    selector: 'album-component',
    templateUrl: 'album.component.html',
    providers  : [MusicService],
    directives : [ROUTER_DIRECTIVES]

})

export class AlbumComponent {

    public album:Album;
    public albumImageSize:AlbumImageSize = AlbumImageSize.LARGE;

    // Constructors
    
    constructor(private musicService:MusicService, private routeParams:RouteSegment) {
        this.getAlbumInfo(this.routeParams.getParam("id"));
    }

    // Actions
    
    getAlbumInfo(query:String) {
        this.musicService.albumInfo(query)
            .subscribe(album => {
                this.album = album;
            })
    }
}
