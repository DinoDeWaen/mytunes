/**
 * Created by serrut on 05/06/16.
 */
import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Router, RouteSegment} from '@angular/router';
import {Album, AlbumImageSize} from './models/album';
import {MusicService} from './services/music.srv';
import {SearchComponent} from "./search.component";
import {PaginationComponent} from './pagination.component';
import {Pager} from '../common/models/pager';

@Component({
    moduleId: module.id,
    selector: 'albums-component',
    templateUrl: 'albums.component.html',
    providers: [MusicService],
    directives: [ROUTER_DIRECTIVES, <any>SearchComponent, <any>PaginationComponent]
})
export class AlbumsComponent implements OnInit {

    public albumImageSize:AlbumImageSize = AlbumImageSize.MEDIUM;

    public queryString:string;
    public albums:Array<Album> = [];
    public pager:Pager;

    // Constructors

    constructor(private musicService:MusicService, private router:Router, private routeSegement:RouteSegment) {
    }

    // Actions

    doSearch($event) {
        this.router.navigate(["/albums", {query: $event}]);
    }

    // OnInit

    ngOnInit() {
        this.queryString = this.routeSegement.getParam("query");
        this.queryString = this.queryString ? decodeURIComponent(this.queryString) : null;
        this.search(this.queryString, 20);
    }

    // Helpers

    private search(query:String, page:number) {
        if (query) {
            this.musicService.albumsSearch(query)
                .subscribe(albums => {
                    this.albums = albums;
                });
        }
    }

}
