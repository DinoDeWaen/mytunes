import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Component({
    moduleId: module.id,
    selector: 'search-component',
    templateUrl: 'search.component.html'
})

export class SearchComponent {

    @Input() value:string;
    @Output() searchChange:EventEmitter<String> = new EventEmitter();

    // Constructors

    constructor() {
        console.log("value: " + this.value);
    }

    // Actions

    onSearch(query:string, event?:KeyboardEvent) {
        if (!event || event.keyCode == 13) {
            this.searchChange.emit(this.value);
        }
    }

}