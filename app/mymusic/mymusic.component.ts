import {Component}                          from '@angular/core';
import {Routes}                             from '@angular/router';

import {MyMusicListComponent}               from './mymusic-list.component';

@Component({
    moduleId: module.id,
    selector: 'mymusic-component',
    templateUrl: `mymusic.component.html`
})

export class MyMusicComponent {
}
