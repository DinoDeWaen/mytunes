/**
 * Created by serrut on 05/06/16.
 */
import {Component} from '@angular/core';
import {UserService} from "./services/user.srv";
import {Router} from '@angular/router';


@Component({
    moduleId: module.id,
    selector: 'login-component',
    templateUrl: 'login.component.html',
    providers : [UserService]
})

export class LoginComponent {
    public error:string;

    constructor(private userService:UserService, private router:Router) {

    }

    login(email:string, password:string) {
        this.userService.login(email, password)
            .subscribe(user => {
                this.router.navigateByUrl("/");
            }, error => {
                this.error = error;
            });
    }
}