import {Component} from '@angular/core';
import {Router} from '@angular/router';

import {UserService} from './services/user.srv';
import {CountryService} from "../common/services/country.srv";
import {Country} from "../common/models/country";


@Component({
    moduleId: module.id,
    selector : 'registration-component',
    templateUrl : 'registration.component.html',
    providers : [UserService, CountryService]
})
export class RegistrationComponent {

    public error:string;
    public countries:Array<Country> = [];

    constructor(private userService:UserService, private router:Router, private countryService:CountryService) {
        this.countryService.getCountries()
            .subscribe(countries => {
                this.countries = countries;
            })
    }

    register(email:string, password:string) {
        console.log(email + " " +password)
        this.userService.register(email, password)
            .subscribe(user => {
                this.router.navigateByUrl("/");
            }, error => {
                this.error = error;
            });
    }
}
